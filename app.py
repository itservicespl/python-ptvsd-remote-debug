import ptvsd

import time
from flask import Flask


app = Flask(__name__)

@app.route('/')
def hello():
    count = 1
    return 'Hello World! I have been seen {} times oha.\n'.format(count)


if __name__ == '__main__':

    ptvsd.enable_attach(address=('0.0.0.0', 5678))
    ptvsd.wait_for_attach()

    app.run(host="0.0.0.0", port="5000", debug=True)